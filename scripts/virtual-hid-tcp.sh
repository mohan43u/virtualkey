#!/usr/bin/env bash
test "${#}" -lt 2 && { echo "[usage] $0 <port> <pipe-to-virtualkeyboard>" 1>&2; exit 1; }

port="${1}"
pipe="${2}"
name=$(basename "${0}")
pidfile=/var/run/"${name}".pid

test -f "${pidfile}" && { echo "${name} already running. If you think otherwise, then remove ${pidfile} and run this script again" 1>&2; exit 1; } || touch "${pidfile}"
ip=$(ip addr show dev $(cat /proc/net/route | awk '$2 ~ /^0+$/{print $1}') | grep '^ *inet ' | awk '{print $2}')

echo "${ip}:${port}"
test ! "x$(which nc 2>/dev/null)" = "x" && { nc -l -p "${port}" >"${pipe}"; rm "${pidfile}"; exit 0; }
test ! "x$(which socat 2>/dev/null)" = "x" && { socat TCP-LISTEN:"${port}",reuseaddr,fork OPEN:"${pipe}",wronly; rm "${pidfile}"; exit 0; }
