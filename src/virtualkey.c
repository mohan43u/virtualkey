/*
 * virtualkey.c - virtual keyboard driver
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* standard headers */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <wchar.h>
#include <iconv.h>

/* X11 headers */
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xproto.h>
#include <X11/keysym.h>
#include <X11/extensions/XI.h>
#include <X11/extensions/XIproto.h>
#include <xorg-server.h>
#include <xf86.h>
#include <xf86Xinput.h>
#include <xf86_OSproc.h>

/* module headers  */
#include "virtualkey.h"
#include "unicode-to-keysym.h"
#include "modmap.h"

static char drivername[] = "virtualkey";
static int VirtualkeyPreInit(InputDriverPtr drv, InputInfoPtr pInfo, int flags);
static void VirtualkeyUnInit(InputDriverPtr drv, InputInfoPtr pInfo, int flags);
static Bool VirtualkeyControl(DeviceIntPtr device, int what);
static void* VirtualkeySetupProc(void* module, void* options, int *errmaj, int *errmin);
static void VirtualkeyTearDownProc(void *);
static void VirtualkeyReadInput(InputInfoPtr pInfo);


XF86ModuleVersionInfo VirtualkeyVersionInfo = {
  drivername,
  MODULEVENDORSTRING,
  MODINFOSTRING1,
  MODINFOSTRING1,
  XORG_VERSION_CURRENT,
  PACKAGE_VERSION_MAJOR,
  PACKAGE_VERSION_MINOR,
  PACKAGE_VERSION_PATCHLEVEL,
  ABI_CLASS_XINPUT,
  ABI_XINPUT_VERSION,
  MOD_CLASS_XINPUT,
  {0, 0, 0, 0}
};

_X_EXPORT XF86ModuleData virtualkeyModuleData = {
  &VirtualkeyVersionInfo,
  VirtualkeySetupProc,
  VirtualkeyTearDownProc
};

_X_EXPORT InputDriverRec VIRTUALKEY = {
  1,
  drivername,
  NULL,
  VirtualkeyPreInit,
  VirtualkeyUnInit,
  NULL,
  NULL,
  0
};

static void* VirtualkeySetupProc(void* module, void* options, int *errmaj, int *errmin) {
  xf86Msg(X_DEBUG, "%s: in SetupProc\n", PACKAGE);
  xf86AddInputDriver(&VIRTUALKEY, module, 0);
  return module;
}

static void VirtualkeyTearDownProc(void *p) {
  xf86Msg(X_DEBUG, "%s: in TearDownProd\n", PACKAGE);
}

static int VirtualkeyPreInit(InputDriverPtr drv, InputInfoPtr pInfo, int flags) {
  VirtualkeyPtr pVirtualkey;
  XkbRMLVOSet rmlvo;

  xf86Msg(X_DEBUG, "%s: in PreInit\n", PACKAGE);
  if(!(pVirtualkey = calloc(sizeof(Virtualkey), 1))) {
    xf86Msg(X_ERROR, "%s: cannot allocate memory for struct Virtualkey. errono: %d\n", PACKAGE, errno);
    goto fail;
  }

  xf86CollectInputOptions(pInfo, NULL);
  xf86ProcessCommonOptions(pInfo, pInfo->options); 
  if(!(pVirtualkey->pipefile = xf86SetStrOption(pInfo->options, "PipeFile", NULL))) {
    xf86Msg(X_ERROR, "%s: no PipeFile option in xorg configuration file\n", PACKAGE);
    goto fail_with_free;
  }
  xf86Msg(X_DEBUG, "%s: PipeFile: %s\n", PACKAGE, pVirtualkey->pipefile);

  pInfo->private = pVirtualkey;
  pInfo->type_name = XI_KEYBOARD;
  pInfo->device_control = VirtualkeyControl;
  pInfo->read_input = VirtualkeyReadInput;

  if(mkfifo(pVirtualkey->pipefile, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH) == -1) {
    xf86Msg(X_ERROR, "%s: cannot create pipefile. errno : %d\n", PACKAGE, errno);
    goto fail_with_free;
  }

 success:
  return Success;

 fail_with_free:
  free(pInfo->private);
 fail:
  pInfo->private = NULL;
  return BadValue;
}

static void VirtualkeyUnInit(InputDriverPtr drv, InputInfoPtr pInfo, int flags) {
  xf86Msg(X_DEBUG, "%s: in UnInit\n", PACKAGE);
  if(pInfo->fd != -1) {
    if(unlink(((VirtualkeyPtr) pInfo->private)->pipefile) == -1) {
      xf86Msg(X_ERROR, "%s: cannot remove pipefile. errno : %d\n", PACKAGE, errno);
    }
  }
  free(pInfo->private);
  pInfo->private = NULL;
  xf86DeleteInput(pInfo, 0);
}

static Bool VirtualkeyControl(DeviceIntPtr device, int what) {
  VirtualkeyPtr pVirtualkey;
  InputInfoPtr pInfo;

  pInfo = device->public.devicePrivate;
  pVirtualkey = pInfo->private;
  
  switch(what) {
  case DEVICE_INIT: {
    xf86Msg(X_DEBUG, "%s: in DEVICE_INPUT\n", PACKAGE);
    if((pInfo->fd = open(pVirtualkey->pipefile, O_RDWR)) == -1) {
      xf86Msg(X_ERROR, "%s: cannot open pipefile. errno: %d\n", PACKAGE, errno);
      goto fail;
    }

    if(!InitKeyboardDeviceStruct(device, NULL, NULL, NULL)) {
      xf86Msg(X_ERROR, "%s: device registration failed\n", PACKAGE, errno);
      goto fail;
    }
  }
    break;
  case DEVICE_ON: {
    xf86Msg(X_DEBUG, "%s: in DEVICE_ON\n", PACKAGE);
    if(pInfo->fd != -1) {
      xf86FlushInput(pInfo->fd);
      AddEnabledDevice(pInfo->fd);
      device->public.on = TRUE;
    }
  }
    break;
  case DEVICE_OFF: {
    xf86Msg(X_DEBUG, "%s: in DEVICE_OFF\n", PACKAGE);
    RemoveEnabledDevice(pInfo->fd);
    device->public.on = FALSE;
  }
    break;
  case DEVICE_CLOSE: {
    xf86Msg(X_DEBUG, "%s: in DEVICE_CLOSE\n", PACKAGE);
    if(pInfo->fd != -1) {
      if(close(pInfo->fd) == -1) {
	xf86Msg(X_ERROR, "%s: cannot close pipefile. errno: %d\n", PACKAGE, errno);
	goto fail;
      }
    }
  }
    break;
  default: {
    return BadValue;
  }
  }
  return Success;

 fail:
  return BadValue;
}

static void VirtualkeyReadInput(InputInfoPtr pInfo) {
  VirtualkeyPtr pVirtualkey = (VirtualkeyPtr) pInfo->private;
  unsigned char buffer[512];
  wchar_t wcsbuffer[512 * 2];
  iconv_t converter;
  char *inptr = buffer;
  char *outptr = (char *) wcsbuffer;
  size_t i, bytes = 0, inbytes = 0, outbytes = sizeof(wcsbuffer);
  KeySym keysym;
  KeyCode keycode, modifier;

  if((converter = iconv_open("WCHAR_T", "UTF-8")) == (iconv_t) -1) {
    xf86Msg(X_ERROR, "%s: iconv_open() failed. errno: %d\n", PACKAGE, errno);
    return;
  }

  memset(buffer, 0, sizeof(buffer));
  memset(wcsbuffer, 0, sizeof(wcsbuffer));
  if((bytes = inbytes = read( pInfo->fd, buffer, sizeof(buffer))) > 0) {
    if(iconv(converter, NULL, NULL, NULL, NULL) == -1) {
      xf86Msg(X_ERROR, "%s: iconv() failed. errno: %d\n", PACKAGE, errno);
      goto clean;
    }
    if(iconv(converter, &inptr, &inbytes, &outptr, &outbytes) == -1) {
      xf86Msg(X_ERROR, "%s: iconv() failed. errno: %d\n", PACKAGE, errno);
      goto clean;
    }
    if(iconv(converter, NULL, NULL, (char **) &wcsbuffer, &outbytes) == -1) {
      xf86Msg(X_ERROR, "%s: iconv() failed. errno: %d\n", PACKAGE, errno);
      goto clean;
    }
    for(i = 0; i < ((sizeof(wcsbuffer) - outbytes)/sizeof(wchar_t) - 1); i++) {
      keysym = 0;
      keycode = 0;
      modifier = 0;
      keysym = UnicodeToKeysym(wcsbuffer[i]);
      keycode = LocalModMap(keysym, &modifier);
      xf86Msg(X_DEBUG, "%s: keysym: 0x%x, keycode: %d, modifier: 0x%x\n", PACKAGE, keysym, keycode, modifier);
      if(modifier) xf86PostKeyboardEvent(pInfo->dev, modifier, 1);
      if(keycode) {
	xf86PostKeyboardEvent(pInfo->dev, keycode, 1);
	xf86PostKeyboardEvent(pInfo->dev, keycode, 0);
      }
      if(modifier) xf86PostKeyboardEvent(pInfo->dev, modifier, 0);
    }
  }
 clean:
  if(iconv_close(converter) == -1) {
    xf86Msg(X_ERROR, "%s: iconv_close() failed. errno: %d\n", PACKAGE, errno);
  }
  return;
}
