/*
 * virtualkey.h - header for private struct
 */

#ifndef __VIRTUALKEY__
#define __VIRTUALKEY__

typedef struct {
  char *pipefile;
} Virtualkey, *VirtualkeyPtr;

#endif /* __VIRTUALKEY__ */
