virtualkey_drv.so
-----------------

virtual keyboard driver for Xorg server.

1. opens a names pipe for communication
2. sends KeyEvents for the incoming bytes
3. capable of creating multiple virtualkeyboard devices in X server.

virtual-hid-tcp.sh
------------------

script which will listen on incoming connections and can communicate with virtualkey driver.

1. runs single instance at anytime

compilation
------------

1. git clone https://github.com/mohan43u/virtualkey.git
2. cd virtualkey
3. ./configure
4. make

example virtual-hid-tcp.h commandline
--------------------------------------

	$ ./virtual-hid-tcp.h 4444 /var/run/virtualkey/virtualkeyboard0.pipe